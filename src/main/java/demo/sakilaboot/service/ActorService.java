package demo.sakilaboot.service;

import com.google.common.collect.Lists;
import demo.sakilaboot.domain.Actor;
import demo.sakilaboot.repository.ActorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ActorService {

    @Autowired
    private ActorRepository actorRepository;

    public List<Actor> list() {
        return Lists.newArrayList(actorRepository.findAll());
    }

    public Optional<Actor> get(int actorId) {
        return actorRepository.findById(actorId);
    }

    public Actor create(Actor actor) {
        return actorRepository.save(actor);
    }

    public Actor update(int actorId, Actor actor) {
        return actorRepository.save(actor);
    }

    public void delete(int actorId) {
        actorRepository.delete(actorRepository
                .findById(actorId).get());
    }

    public void deleteAll() {
        actorRepository.deleteAll();
    }

}
