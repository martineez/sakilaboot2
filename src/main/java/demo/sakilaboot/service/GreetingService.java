package demo.sakilaboot.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class GreetingService {
    private static final Logger logger = LoggerFactory.getLogger(GreetingService.class);

    public String greet(String name) {
        logger.trace("service greets");
        return String.format("Hello %s!", name);
    }
}