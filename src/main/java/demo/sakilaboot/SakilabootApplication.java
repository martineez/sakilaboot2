package demo.sakilaboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SakilabootApplication {

	public static void main(String[] args) {
		SpringApplication.run(SakilabootApplication.class, args);
	}
}
