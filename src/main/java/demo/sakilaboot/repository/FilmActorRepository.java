package demo.sakilaboot.repository;

import demo.sakilaboot.domain.FilmActor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FilmActorRepository extends JpaRepository<FilmActor, Integer> {

}
