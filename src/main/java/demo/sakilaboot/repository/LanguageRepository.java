package demo.sakilaboot.repository;

import demo.sakilaboot.domain.Language;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LanguageRepository extends JpaRepository<Language, Byte> {

}
