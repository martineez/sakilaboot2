package demo.sakilaboot.repository;

import demo.sakilaboot.domain.Actor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource
public interface ActorRepository extends JpaRepository<Actor, Integer> {
    List<Actor> findByFirstName(@Param("firstName") String firstName);
    List<Actor> findByLastName(@Param("lastName") String lastName);
}
