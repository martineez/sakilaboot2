package demo.sakilaboot.repository;

import demo.sakilaboot.domain.Store;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StoreRepository extends JpaRepository<Store, Byte> {

}
