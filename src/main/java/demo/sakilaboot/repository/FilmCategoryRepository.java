package demo.sakilaboot.repository;

import demo.sakilaboot.domain.FilmCategory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FilmCategoryRepository extends JpaRepository<FilmCategory, Byte> {

}
