package demo.sakilaboot.repository;

import demo.sakilaboot.domain.City;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CityRepository extends JpaRepository<City, Integer> {

}
