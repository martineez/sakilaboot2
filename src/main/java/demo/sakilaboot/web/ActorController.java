package demo.sakilaboot.web;

import demo.sakilaboot.domain.Actor;
import demo.sakilaboot.service.ActorService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path="/actors")
public class ActorController {

    private final ActorService service;

    public ActorController(ActorService service) {
        this.service = service;
    }

    // ------------------- Retrieve all actors ---------------------------------------------
    @GetMapping(path="/")
    public ResponseEntity<List<Actor>> getAllActors() {
        return new ResponseEntity(service.list(), HttpStatus.OK);
    }

    // ------------------- Retrieve a single actor ------------------------------------------
    @GetMapping(value = "/{id}")
    public ResponseEntity<Actor> getActor(@PathVariable("id") int id) {
        Optional<Actor> actor = service.get(id);

        if (actor == null) {
            return new ResponseEntity("No Actor found for ID " + id, HttpStatus.NOT_FOUND);
        }

//        return new ResponseEntity(actor, HttpStatus.FOUND);
        return ResponseEntity
                .status(HttpStatus.FOUND)
                .body(actor.get());
    }

    // ------------------- Create an actor -------------------------------------------
    @PostMapping(value = "/")
    public ResponseEntity<Actor> addNewActor (@RequestBody Actor actor) {
        service.create(actor);
        return new ResponseEntity<>(actor, HttpStatus.CREATED);
    }

    // ------------------- Update an actor ------------------------------------------------
    @PutMapping(value = "/{id}")
    public ResponseEntity<?> updateActor(@PathVariable("id") int id, @RequestBody Actor actor) {
        actor = service.update(id, actor);

        if (null == actor) {
            return new ResponseEntity("No Actor found for ID " + id, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity(actor, HttpStatus.OK);
        
    }
    
    // ------------------- Delete a single actor ------------------------------------------
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> deleteActor(@PathVariable("id") int id) {
        try {
            service.delete(id);
        }
        catch (IllegalArgumentException ex) {
            return new ResponseEntity("No Actor found for ID " + id, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity(id, HttpStatus.OK);

    }

    // ------------------- Delete all actors -----------------------------
    @DeleteMapping(value = "/")
    public ResponseEntity<Actor> deleteAllActors() {
        service.deleteAll();
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}