package demo.sakilaboot.web;

import demo.sakilaboot.service.GreetingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {

    private static final Logger logger = LoggerFactory.getLogger(GreetingController.class);

    private final GreetingService service;

    public GreetingController(GreetingService service) {
        logger.trace("home controller ctor");
        this.service = service;
    }

    @RequestMapping("/greeting")
    public String greeting(@RequestParam(defaultValue = "Sakila") String name) {
        logger.trace("home controller greets by service");
        return service.greet(name);
    }
}
