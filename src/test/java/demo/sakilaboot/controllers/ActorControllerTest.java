package demo.sakilaboot.controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import demo.sakilaboot.domain.Actor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ActorControllerTest extends AbstractTestNGSpringContextTests {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    public void testGetAllActors() throws Exception {
//        testRestTemplate.getRestTemplate().getMessageConverters().add()
                
        ResponseEntity<List<Actor>> responseEntity = testRestTemplate.exchange("/api/actors/",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Actor>>(){});
        System.out.println(responseEntity);
//        assertThat(responseEntity.getBody())
//                .contains("The HAL Browser (for Spring Data REST)");
    }

    @Test
    public void testGetActorAsPOJO() {
        Actor actor = this.testRestTemplate
                .getForObject("http://localhost:" + port + "/api/actors/1", Actor.class);

        assertThat(actor.getFirstName(), is("PENELOPE"));
        assertThat(actor.getLastName(), is("GUINESS"));
    }

    @Test
    public void testGetActorAsJSON() throws IOException {

        ResponseEntity<String> response
                = testRestTemplate.getForEntity("http://localhost:" + port + "/api/actors/1", String.class);

        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = mapper.readTree(response.getBody());
        JsonNode firstName = root.path("firstName");
        JsonNode lastName = root.path("lastName");
        
        assertThat(firstName.asText(), is("PENELOPE"));
        assertThat(lastName.asText(), is("GUINESS"));
    }

    @Test
    public void testFindByFirstName() throws Exception {

        ResponseEntity<String> response
                = testRestTemplate.getForEntity("http://localhost:" + port + "/api/actors/search/findByFirstName?firstName=LUCILLE", String.class);

        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
    }

    @Test
    public void testAddNewActor() throws Exception {
    }

    @Test
    public void testUpdateActor() throws Exception {
    }

    @Test
    public void testDeleteActor() throws Exception {
    }

    @Test
    public void testDeleteAllActors() throws Exception {
    }

}