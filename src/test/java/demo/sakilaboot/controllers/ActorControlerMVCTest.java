package demo.sakilaboot.controllers;

import demo.sakilaboot.domain.Actor;
import demo.sakilaboot.service.ActorService;
import demo.sakilaboot.web.ActorController;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.springframework.hateoas.MediaTypes;

import java.util.Arrays;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@WebMvcTest(ActorController.class)
public class ActorControlerMVCTest extends AbstractTestNGSpringContextTests {

    @InjectMocks
    ActorController controllerUnderTest;

    @Autowired
    private MockMvc mvc;

    @Mock
    private ActorService mockService;

    @BeforeTest
    public void setup() {

        // this must be called for the @Mock annotations above to be processed
        // and for the mock service to be injected into the controller under
        // test.
        MockitoAnnotations.initMocks(this);

        this.mvc = MockMvcBuilders.standaloneSetup(
//                new ActorController(mockService))
                controllerUnderTest)
                .build();               

    }

    @Test
    public void findAllActorsShouldReturnFoundActorEntries() throws Exception {
        Actor first = new Actor("A", "B");
        Actor second = new Actor("C", "D");

        when(this.mockService.list())
                .thenReturn(Arrays.asList(first, second));

        this.mvc.perform(get("/api/actors"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaTypes.HAL_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(2)))
//                .andExpect(jsonPath("$[0].id", is(1)))
//                .andExpect(jsonPath("$[0].description", is("Lorem ipsum")))
//                .andExpect(jsonPath("$[0].title", is("Foo")))
//                .andExpect(jsonPath("$[1].id", is(2)))
//                .andExpect(jsonPath("$[1].description", is("Lorem ipsum")))
                .andExpect(jsonPath("$[1].title", is("Bar")));

        verify(mockService, times(1)).list();
        verifyNoMoreInteractions(mockService);
    }

    @Test
    public void shouldReturnDefaultMessage() throws Exception {
        this.mvc.perform(get("/api/actors"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("The HAL Browser (for Spring Data REST)")));
    }
}
