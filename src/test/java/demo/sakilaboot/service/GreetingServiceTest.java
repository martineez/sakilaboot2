package demo.sakilaboot.service;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class GreetingServiceTest {

    private static GreetingService service;

    @BeforeClass
    public static void setup() {
        service = new GreetingService();
    }

    @Test
    public void greetTest() {
        assertThat(service.greet("User")).isEqualTo("Hello User!");
    }
}