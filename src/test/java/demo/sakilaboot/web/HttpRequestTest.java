package demo.sakilaboot.web;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class HttpRequestTest {

//    @LocalServerPort
//    private int port;

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    public void greetingShouldReturnDefaultMessage() {
        ResponseEntity<String> response = testRestTemplate.getForEntity("/greeting", String.class);

        assertThat(response.getBody()).isEqualTo("Hello Sakila!");
    }

    @Test
    public void halBrowserShouldWork() {
        String body = this.testRestTemplate.getForObject("/api/browser/index.html#/api", String.class);

        assertThat(body.contains("The HAL Browser (for Spring Data REST)"));
    }
}