package demo.sakilaboot;

import demo.sakilaboot.web.ActorController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class TestNGSmokeTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private ActorController actorController;

    @Test
    public void contexLoads() throws Exception {
        assertThat(actorController).isNotNull();
    }
}
