package demo.sakilaboot.domain;

import demo.sakilaboot.repository.AddressRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AddressRepositoryIntegrationTests {
    @Autowired
    AddressRepository repository;

    @Test
    public void allAddressesShouldBe603() {
        Page<Address> addresses = this.repository.findAll(PageRequest.of(0, 10));
        assertThat(addresses.getTotalElements()).isEqualTo(603L);
    }
}