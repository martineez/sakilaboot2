package demo.sakilaboot.domain;

import demo.sakilaboot.repository.ActorRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@SpringBootTest
public class ActorRepositoryIntegrationTests {
    @Autowired
    ActorRepository repository;

    @Test
    public void allActorsShouldBe200() {
        Page<Actor> actors = this.repository.findAll(PageRequest.of(0, 10));
        assertThat(actors.getTotalElements()).isEqualTo(200L);
    }

    @Test
    public void findByFirstNameShouldBe1() {
        List<Actor> actors = this.repository.findByFirstName("JENNIFER");
        assertThat(actors.size()).isEqualTo(1);
    }

    @Test
    public void findByLastNameShouldBe1() {
        List<Actor> actors = this.repository.findByLastName("DAVIS");
        assertThat(actors.size()).isEqualTo(3);
    }


}
