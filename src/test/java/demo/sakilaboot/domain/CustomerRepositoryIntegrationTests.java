package demo.sakilaboot.domain;
import demo.sakilaboot.repository.CustomerRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerRepositoryIntegrationTests {
    @Autowired
    CustomerRepository repository;

    @Test
    public void allCustomersShouldBe600() {
        Page<Customer> customers = this.repository.findAll(PageRequest.of(0, 10));
        assertThat(customers.getTotalElements()).isEqualTo(599L);
    }
}
